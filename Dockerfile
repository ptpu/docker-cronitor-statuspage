FROM node:17

RUN apt-get update && apt-get upgrade -y

RUN git clone https://github.com/cronitorio/cronitor-statuspage.git /app
WORKDIR /app

ARG CRONITOR_API_KEY
ARG NEXT_PUBLIC_NAME
ARG NEXT_PUBLIC_LOGO
ARG NEXT_PUBLIC_WEBSITE_URL
ARG NEXT_PUBLIC_CTA_TITLE
ARG NEXT_PUBLIC_CTA_URL

RUN yarn && yarn build

EXPOSE 3000
CMD ["yarn", "start"]